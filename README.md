dllp
====

Driving license learning platform

Requirements
============
-) ruby
-) gems
-) rails (> 4)

INSTALL and RUN
===============
cd dllp
rake db:setup
rails s

Open a Webbrowser and go to http://localhost:3000

Example Login credentials
=========================

Username: user@example.org
Password: test1234

