// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require jquery-ui
//= require chart
//= require legend
//= require_self


var animationduration = 400;

function addKeyBindings() {
    $("body").keypress(function(event) {
        switch (event.which) {
            case 49:
                if ($('#cb1').is(':checked'))
                    $('#cb1').removeAttr('checked');
                else {
                    $('#cb1').prop('checked', true);
                }
                break;
            case 50:
                if ($('#cb2').is(':checked'))
                    $('#cb2').removeAttr('checked');
                else {
                    $('#cb2').prop('checked', true);
                }
                break;
            case 51:
                if ($('#cb3').is(':checked'))
                    $('#cb3').removeAttr('checked');
                else {
                    $('#cb3').prop('checked', true);
                }
                break;
            case 52:
                if ($('#cb4').is(':checked'))
                    $('#cb4').removeAttr('checked');
                else {
                    $('#cb4').prop('checked', true);
                }
                break;
            case 13:
                checkAnswer();
        }
    });
}

$(document).ready(function() {
    $("#info-message").hide();
    $("#error-message").hide();
    $("#success-message").hide();
});