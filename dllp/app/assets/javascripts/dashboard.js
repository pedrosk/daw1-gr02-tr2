var correct = [];
var incorrect = [];
var learned = [];
var unanswered = [];
var labels = [];


$(document).ready(function() {
    var ctx = $("#history-canvas")[0].getContext("2d");

    var data = {
        labels: labels,
        datasets: [
            {
                label: "Correct",
                fillColor: "rgba(90, 162, 204, 0.1)",
                strokeColor: "rgb(90, 162, 204)",
                pointColor: "rgb(90, 162, 204)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: correct
            },
            {
                label: "Learned",
                fillColor: "rgba(164, 219, 121, 0.8)",
                strokeColor: "rgb(164, 219, 121)",
                pointColor: "rgb(164, 219, 121)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: learned
            },
            {
                label: "Unanswered",
                fillColor: "rgba(0,0,0,0)",
                strokeColor: "rgba(0,0,0,1)",
                pointColor: "rgba(0,0,0,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: unanswered
            },
            {
                label: "Incorrect",
                fillColor: "rgb(239, 41, 41)",
                strokeColor: "rgb(239, 41, 41)",
                pointColor: "rgb(239, 41, 41)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: incorrect
            },
        ]
    };
    var historyChart = new Chart(ctx).Line(data, null);
    legend(document.getElementById("legend"), data);
});
