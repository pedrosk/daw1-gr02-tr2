function checkAnswer() {
    $("#info-message").show(animationduration);
    var answers = {};

    answers[$('#cb1').prop('value')] = !!$('#cb1').prop('checked');
    answers[$('#cb2').prop('value')] = !!$('#cb2').prop('checked');
    answers[$('#cb3').prop('value')] = !!$('#cb3').prop('checked');
    answers[$('#cb4').prop('value')] = !!$('#cb4').prop('checked');

    $.post($("#form-answer").attr('action'), {answers: answers}, function (data) {
        $("#info-message").hide(animationduration);
        $("#result").html(data);
    }, 'html').fail(function() {
        $("#info-message").hide(animationduration);
        $("#error-message").text("Unexpected error occurred, please try again later.");
        $("#error-message").show(animationduration);
    });
}

ready = function() {
    addKeyBindings();

    $("#result").on('click', '.btn-check', function(){
        checkAnswer();
    });
};

$(document).ready(ready);

