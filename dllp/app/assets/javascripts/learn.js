var state = "check";

function checkAnswer() {
    $("#info-message").show(animationduration);
    var answers = {};

    answers[$('#cb1').prop('value')] = !!$('#cb1').prop('checked');
    answers[$('#cb2').prop('value')] = !!$('#cb2').prop('checked');
    answers[$('#cb3').prop('value')] = !!$('#cb3').prop('checked');
    answers[$('#cb4').prop('value')] = !!$('#cb4').prop('checked');


    $.post($("#form-answer").attr('action'), {answers: answers}, function (data) {
        $("#info-message").hide(animationduration);
        if (data[$('#cb1').prop('value')] == true)
            $("#answer1").addClass("callout-success");
        else
            $("#answer1").addClass("callout-danger");
        if (data[$('#cb2').prop('value')] == true)
            $("#answer2").addClass("callout-success");
        else
            $("#answer2").addClass("callout-danger");
        if (data[$('#cb3').prop('value')] == true)
            $("#answer3").addClass("callout-success");
        else
            $("#answer3").addClass("callout-danger");
        if (data[$('#cb4').prop('value')] == true)
            $("#answer4").addClass("callout-success");
        else
            $("#answer4").addClass("callout-danger");
        state = "next";
    }).fail(function() {
        $("#info-message").hide(animationduration);
        $("#error-message").text("Unexpected error occurred, please try again later.");
        $("#error-message").show(animationduration);
    });

    $(".btn-check").hide();
    $(".btn-next").removeClass('hidden');
}

ready = function() {
    $("body").keypress(function(event) {
        switch (event.which) {
            case 49:
                if ($('#cb1').is(':checked'))
                    $('#cb1').removeAttr('checked');
                else {
                    $('#cb1').prop('checked', true);
                }
                break;
            case 50:
                if ($('#cb2').is(':checked'))
                    $('#cb2').removeAttr('checked');
                else {
                    $('#cb2').prop('checked', true);
                }
                break;
            case 51:
                if ($('#cb3').is(':checked'))
                    $('#cb3').removeAttr('checked');
                else {
                    $('#cb3').prop('checked', true);
                }
                break;
            case 52:
                if ($('#cb4').is(':checked'))
                    $('#cb4').removeAttr('checked');
                else {
                    $('#cb4').prop('checked', true);
                }
                break;
            case 13:
                if (state == "check")
                    checkAnswer();
                else
                    document.location.href = "/learn";
                break;
        }
    });

    $("#result").on('click', '.btn-check', function(){
        checkAnswer();
    });
};

$(document).ready(ready);

