class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include AuthenticationsHelper

  private

  def require_login
    unless logged_in?
      redirect_to root_path
    end
  end

  def require_setup
    unless logged_in?
      redirect_to root_path
    end
  end

end
