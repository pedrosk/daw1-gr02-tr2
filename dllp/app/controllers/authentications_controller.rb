require 'securerandom'
class AuthenticationsController < ApplicationController
    def index
        login
    end

    def login
        if logged_in?
            redirect_to '/dashboard'
        else
            @visible_form = 'form-login'
            @categories = Category.all
            render 'index.html.erb'
        end
    end

    def logout
        log_out
        redirect_to root_url
    end

    def register
        @visible_form = 'form-register'
        @categories = Category.all
        render 'index.html.erb'
    end

    def forgot
        @visible_form = 'form-forgot'
        @categories = Category.all
        render 'index.html.erb'
    end

    def do_login
        success = false
        error = ''
        user = User.find_by(email: params[:user][:email].downcase)
        if user && user.authenticate(params[:user][:password])
            success = true
            log_in user
            LearnHistory.update(user)
        else
            error = 'Invalid username or password'
        end
        render json: {:success => success, :error => error }
    end

    def do_signup
        success = false
        error = ''

        if params.has_key?(:categories)
            user = User.new({
                                email: params[:user][:email].downcase,
                                password: params[:user][:password],
                                password_confirmation: params[:user][:password_confirmation]
                            })
            if user.valid?
                user.categories << Category.find(params[:categories])
                user.save
                success = true
            else
                error = user.errors.full_messages.to_sentence
            end
        else
          error = 'You must select at least one category'
        end

        render json: {:success => success, :error => error }
    end

    def do_forgot
        user = User.find_by_email(params[:email])
        success = false
        error = ''
        if user != nil
            success = true
            random_password = SecureRandom.base64(10)
            user.password = random_password
            user.password_confirmation = random_password
            user.save
            UserMailer.password_reset(user, random_password)
        else
            error = "Sorry, but we have no user for #{params[:email]}"
        end
        render json: {:success => success, :error => error }
    end

end
