class DashboardController < ApplicationController
  before_action :require_login
  def index
    history = LearnHistory.where(:user_id => current_user.id)

    # crunching chart numbers
    @labels = history.map { |x| x[:created_at].strftime('%d.%m.%Y') }.to_json.html_safe
    @correct = history.map { |x| x[:correct]}.to_json
    @incorrect = history.map { |x| x[:incorrect]}.to_json
    @unanswered = history.map { |x| x[:unanswered]}.to_json
    @learned = history.map { |x| x[:learned]}.to_json

    # total progress numbers
    @cnt_total_to_learn = Question.unanswered(current_user).count + Question.incorrect(current_user).count
    @cnt_total_done = Question.correct(current_user).count + Question.learned(current_user).count
    @progress_total = (@cnt_total_done.to_f / Question.categories(current_user).count.to_f * 100.0).to_int

    # todays progress numbers
    @days_left = [(current_user.examdate - Date.today).to_i,1].max # make sure to prevent div by 0

    last_progress = (history.length >= 2) ? history[-2] : history[-1]

    @last_progress = last_progress
    # make sure to prevent div by 0
    @daily_goal = [(Question.categories(current_user).count - last_progress[:correct] - last_progress[:learned]) / @days_left, 1].max

    @cnt_today_done = Question.today_done(current_user).count
    @cnt_today_to_learn = [@daily_goal - @cnt_today_done,0].max
    @progress_today = (@cnt_today_done.to_f / @daily_goal.to_f * 100.0).to_int

  end



end
