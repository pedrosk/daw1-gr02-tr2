class ExamController < ApplicationController
  include ExamHelper, LearnHelper, ActionView::Helpers::NumberHelper

  EXAM_QUESTIONS = 5
  PASS_LIMIT = 80.0

  def index
    exam = get_or_create_exam(current_user)
    @cnt_done = exam.answer_attempts.count
    @cnt_total = EXAM_QUESTIONS
    @link_suffix = ''
    if @cnt_done < EXAM_QUESTIONS
      @q = Exam.random_question(exam)
    else
      params[:id] = exam.id
      self.result
    end
  end

  def validate
    answers = Answer.where(:id => params[:answers].keys)
    result = Hash.new
    answers.each do |item|
      result[item.id.to_s] = (item[:correct].to_s == params[:answers][item.id.to_s].to_s)
    end
    log_answer(current_user, answers[0].question, !result.has_value?(false))
    log_exam_answer(current_exam, answers[0].question, !result.has_value?(false))

    self.index
  end

  def result
    exam = Exam.find(params[:id])
    #security check
    if exam.user_id != current_user.id
      redirect_to dashboard_url
    end

    @correct_questions = Exam.correct_questions(exam)
    @incorrect_questions = Exam.incorrect_questions(exam)
    @percentage = (@correct_questions.length.to_f / EXAM_QUESTIONS.to_f * 100.0)
    @passed = (@percentage > PASS_LIMIT)

    if @passed
      @color = 'success'
    else
      @color = 'danger'
    end

    finish_exam(exam, @passed, @percentage)

    render :action => :result
  end
end
