class LearnController < ApplicationController
  include LearnHelper
  before_action :require_login

  # Smart practicing
  def index
    if Question.unanswered(current_user).count > 0
      @q = Question.unanswered(current_user).first
    elsif Question.incorrect(current_user).count > 0
      @q = Question.incorrect(current_user).first
    else
      @q = Question.correct(current_user).first
    end

    @cnt_total = Question.categories(current_user).count
    @cnt_done = Question.correct(current_user).count + Question.learned(current_user).count
    @link_suffix = ''

    respond_to do |format|
      format.js
      format.html
    end
  end

  def unanswered
    @q = Question.unanswered(current_user).first
    @cnt_total = Question.categories(current_user).count
    @cnt_done = @cnt_total - Question.unanswered(current_user).count
    @link_suffix = 'unanswered'

    if (@cnt_total == @cnt_done)
      self.finished
    else
      respond_to do |format|
        format.js {render 'index' }
        format.html {render 'index'}
      end
    end
  end

  def incorrect
    @q = Question.incorrect(current_user).first
    @cnt_total = Question.categories(current_user).count
    @cnt_done = @cnt_total - Question.incorrect(current_user).count
    @link_suffix = 'incorrect'

    if (@cnt_total == @cnt_done)
      self.finished
    else
      render 'index'
    end
  end

  def validate
    answers = Answer.where(:id => params[:answers].keys)
    result = Hash.new
    answers.each do |item|
      result[item.id.to_s] = (item[:correct].to_s == params[:answers][item.id.to_s].to_s)
    end
    log_answer(current_user, answers[0].question, !result.has_value?(false))
    render json: result.to_json
  end

  def finished
    render :action => :finished
  end
end
