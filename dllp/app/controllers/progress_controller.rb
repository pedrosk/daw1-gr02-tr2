class ProgressController < ApplicationController
  include ActionView::Helpers::NumberHelper
  before_action :require_login

  def index
    @incorrect = Question.incorrect(current_user)
    @unanswered = Question.unanswered(current_user)
    @correct = Question.correct(current_user)
    @learned = Question.learned(current_user)

    @total = Question.categories(current_user).count
    @exams = Exam.where(:user_id => current_user.id)

  end
end
