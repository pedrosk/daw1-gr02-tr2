class UserController < ApplicationController
  before_action :require_login

  def edit
    @user = current_user
    @categories = Category.all
  end

  def update

    user = current_user
    @error = ''

    if (!params.has_key?(:categories))
      @error = 'You must select at least one driving license type'
    else
      categories = Category.find(params[:categories])
      user.categories.destroy_all
      user.categories << categories

      user.update_attributes(user_params)

      if user.valid?
        user.save
      else
        @error = user.errors.full_messages.to_sentence
      end
    end

    respond_to do |format|
      format.js
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :examdate)
  end

end
