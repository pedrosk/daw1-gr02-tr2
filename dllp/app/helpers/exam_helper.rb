module ExamHelper
  def get_or_create_exam(user)
    if !session.has_key?(:exam_id)
      exam = Exam.create(:user_id => user.id)
      session[:exam_id] = exam.id
    else
      exam = Exam.where(:user_id => user.id).last
    end
    return exam
  end

  def current_exam
    Exam.find(session[:exam_id])
  end

  def finish_exam(exam, passed, result)
    exam.passed = passed
    exam.result = result
    exam.save
    session.delete(:exam_id)
  end

  def log_exam_answer(exam, question, correct)
    cnt_incorrect = 0
    cnt_correct = 0

    if correct
      cnt_correct = 1
    else
      cnt_incorrect = 1
    end

    AnswerAttempt.create(:user_id => exam.user.id, :question_id => question.id, :exam_id => exam.id, correct: cnt_correct, incorrect: cnt_incorrect)

  end
end
