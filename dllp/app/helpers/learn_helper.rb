module LearnHelper
  def log_answer(user, question, correct)
    answer = AnswerAttempt.find_by(user: user, question: question)

    logger.info "Foobar"

    if (answer.nil?)
      answer = AnswerAttempt.new(user: user, question: question, practiced: 0, correct: 0, incorrect: 0)
    end

    answer.practiced = answer.practiced + 1

    if correct == true
      answer.correct = answer.correct + 1
      answer.incorrect = 0
    else
      answer.correct = 0
      answer.incorrect = answer.incorrect + 1
    end
    answer.save
    LearnHistory.update(user)
  end
end
