class Exam < ActiveRecord::Base
  belongs_to :user
  has_many :answer_attempts

  scope :correct_questions, -> exam {
    Question.where(id: AnswerAttempt.where(:exam_id => exam.id).where('correct > 0').select(:question_id))
  }

  scope :incorrect_questions, -> exam {
    Question.where(id: AnswerAttempt.where(:exam_id => exam.id).where('incorrect > 0').select(:question_id))
  }

  def self.random_question(exam)
    Question.categories(exam.user).where.not(:id => AnswerAttempt.where(:user_id => exam.user.id).where(:exam_id => exam.id).select(:question_id)).first
  end
end
