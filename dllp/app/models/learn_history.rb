class LearnHistory < ActiveRecord::Base
  belongs_to :user


  scope :today, -> user {
    where(category_id: user.categories.select(:category_id))
  }

  def self.today(user)
    history = LearnHistory.where(:user_id => user.id).where('DATE(created_at) = DATE(?)', Time.now).first
    if history == nil
      history = LearnHistory.create(:user_id => user.id, :learned => 0, :correct => 0, :incorrect => 0, :unanswered => 0)
    end
    return history
  end

  def self.update(user)
    history = today(user)
    history.unanswered = Question.unanswered(user).count
    history.correct = Question.correct(user).count
    history.incorrect = Question.incorrect(user).count
    history.learned = Question.learned(user).count
    history.save
  end
end
