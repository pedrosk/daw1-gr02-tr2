class Question < ActiveRecord::Base
  belongs_to :category
  has_many :answers
  has_many :answer_attempts

  scope :categories, -> user {
    where(category_id: user.categories.select(:category_id))
  }

  scope :unanswered, -> user {
    categories(user).where.not(id: AnswerAttempt.where(:user_id => user.id).where(:exam_id => nil).select(:question_id))
  }

  scope :incorrect, -> user {
    categories(user).where(id: AnswerAttempt.where(:user_id => user.id).where('incorrect > 0').where(:exam_id => nil).select(:question_id))
  }

  scope :correct, -> user {
    categories(user).where(id: AnswerAttempt.where(:user_id => user.id).where('correct > 0').where('correct < 3').where(:exam_id => nil).select(:question_id))
  }

  scope :learned, -> user {
    categories(user).where(id: AnswerAttempt.where(:user_id => user.id).where('correct  >= 3').where(:exam_id => nil).select(:question_id))
  }

  scope :today_done, -> user {
    categories(user).where(id: AnswerAttempt.where(:user_id => user.id).where('correct > 0').where(:exam_id => nil).where('DATE(updated_at) = DATE(?)', Date.today).select(:question_id))
  }

end
