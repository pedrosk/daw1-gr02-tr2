class User < ActiveRecord::Base
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, 
    presence: true, 
    length: { maximum: 255 }, 
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: { case_sensitive: false }
    
  before_save { self.email = email.downcase }
  has_secure_password
  validates :password, length: { minimum: 5 }, allow_blank: true
  has_and_belongs_to_many :categories

  after_create { LearnHistory.create(:user_id => self.id, learned: 0, correct: 0, incorrect: 0, unanswered: 0)}
end
