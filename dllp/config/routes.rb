Rails.application.routes.draw do

  get 'learn/index'

  get 'learn/validate'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'authentications#index'

  #Authentication
  get   'login' => 'authentications#login'
  get   'signup' => 'authentications#register'
  get   'forgotpassword' => 'authentications#forgot'
  get   'logout' => 'authentications#logout'
  post  'login' => 'authentications#do_login'
  post  'signup' => 'authentications#do_signup'
  post  'forgotpassword' => 'authentications#do_forgot'

  # Dashboard
  get 'dashboard' => 'dashboard#index'

  #Progress
  get 'progress' => 'progress#index'

  # Learning
  get  'learn' => 'learn#index'
  get  'learn/unanswered' => 'learn#unanswered'
  get  'learn/incorrect' => 'learn#incorrect'
  post 'learn' => 'learn#validate'

  # Exam
  get 'exam' => 'exam#index'
  post 'exam' => 'exam#validate'
  get 'exam/result/:id' => 'exam#result'


  # Settings
  get 'user' => 'user#edit'
  post 'user' => 'user#update'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
