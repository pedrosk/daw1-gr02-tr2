class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :title
      t.text :description
      t.string :image_url
      t.references :category, index: true

      t.timestamps
    end
  end
end
