class AddExamDate2User < ActiveRecord::Migration
  def change
    add_column :users, :examdate, :date, :null => false, :default => Time.now + 60.days
  end
end
