class CreateAnswerAttempts < ActiveRecord::Migration
  def change
    create_table :answer_attempts do |t|
      t.integer :correct
      t.integer :incorrect
      t.integer :practiced
      t.references :user, index: true
      t.references :question, index: true

      t.timestamps
    end
  end
end
