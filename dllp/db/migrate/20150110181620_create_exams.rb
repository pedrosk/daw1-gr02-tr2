class CreateExams < ActiveRecord::Migration
  def change
    create_table :exams do |t|
      t.boolean :passed
      t.integer :result
      t.references :user, index: true

      t.timestamps
    end
    add_column :answer_attempts, :exam_id, :integer, :references => 'exam'
    add_index :answer_attempts, :exam_id
  end
end
