class CreateLearnHistories < ActiveRecord::Migration
  def change
    create_table :learn_histories do |t|
      t.references :user, index: true
      t.integer :learned, default: 0
      t.integer :correct, default: 0
      t.integer :incorrect, default: 0
      t.integer :unanswered, default: 0

      t.timestamps
    end
  end
end
