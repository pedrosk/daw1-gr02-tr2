# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150111121649) do

  create_table "answer_attempts", force: true do |t|
    t.integer  "correct"
    t.integer  "incorrect"
    t.integer  "practiced"
    t.integer  "user_id"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "exam_id"
  end

  add_index "answer_attempts", ["exam_id"], name: "index_answer_attempts_on_exam_id"
  add_index "answer_attempts", ["question_id"], name: "index_answer_attempts_on_question_id"
  add_index "answer_attempts", ["user_id"], name: "index_answer_attempts_on_user_id"

  create_table "answers", force: true do |t|
    t.text     "answer"
    t.boolean  "correct"
    t.integer  "question_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id"

  create_table "categories", force: true do |t|
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories_users", id: false, force: true do |t|
    t.integer "category_id"
    t.integer "user_id"
  end

  add_index "categories_users", ["category_id"], name: "index_categories_users_on_category_id"
  add_index "categories_users", ["user_id"], name: "index_categories_users_on_user_id"

  create_table "exams", force: true do |t|
    t.boolean  "passed"
    t.integer  "result"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "exams", ["user_id"], name: "index_exams_on_user_id"

  create_table "learn_histories", force: true do |t|
    t.integer  "user_id"
    t.integer  "learned"
    t.integer  "correct"
    t.integer  "incorrect"
    t.integer  "unanswered"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "learn_histories", ["user_id"], name: "index_learn_histories_on_user_id"

  create_table "questions", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "image_url"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "questions", ["category_id"], name: "index_questions_on_category_id"

  create_table "users", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.date     "examdate",        default: '2015-03-11', null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
