# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

c1 = Category.create({:title => 'B: Car driving license'})


q1 = Question.create({:title => 'Dangers', :description => 'The cars before you get slower. What do you do?', :image_url => '1.gif',  :category => c1})
Answer.create({answer: 'I won\'t let the service car overtake me', correct: false, question: q1 })
Answer.create({answer: 'I keep as left as possible without danger', correct: true, question: q1 })
Answer.create({answer: 'I keep distance of at least 2 seconds', correct: true, question: q1 })
Answer.create({answer: 'I keep as right as possible', correct: false, question: q1 })

q2 = Question.create({:title => 'Signs', :description => 'How fast are you allowed to drive?', :image_url => '2.jpg',  :category => c1})
Answer.create({answer: '30 km/h tops', correct: true, question: q2 })
Answer.create({answer: 'At least 30 km/h', correct: false, question: q2 })
Answer.create({answer: '30 km/h at average', correct: false, question: q2 })
Answer.create({answer: 'Only step speed', correct: false, question: q2 })

q3 = Question.create({:title => 'Signs', :description => 'What refers this sign to?', :image_url => '3.jpg',  :category => c1})
Answer.create({answer: 'A pedestrian and bicycle walk', correct: false, question: q3 })
Answer.create({answer: 'Bicycle lane', correct: false, question: q3 })
Answer.create({answer: 'Bicycle walk', correct: false, question: q3 })
Answer.create({answer: 'Bicycle cross way', correct: true, question: q3 })

q4 = Question.create({:title => 'Crossings', :description => 'Sie befinden sich in einer Nebenfahrbahn und wollen in die Kreuzung einfahren. Gilt die Ampel auch für Sie?', :image_url => '4.jpg',  :category => c1})
Answer.create({answer: 'Ja, die Ampel gilt für die gesamte Straße', correct: true, question: q4 })
Answer.create({answer: 'Ja, nur wenn ich auf die Hauptfahrbahn fahre', correct: false, question: q4 })
Answer.create({answer: 'Nein, da die Ampel nur für die Hauptfahrbahn gilt', correct: false, question: q4 })
Answer.create({answer: 'Ja, nur wenn ich in der Nebenfahrbahn weiterfahre', correct: false, question: q4 })

q5 =Question.create({:title => 'Signs', :description => 'Was kündigt dieses Verkehrszeichen mit Zusatztafel an?', :image_url => '5.jpg',  :category => c1})
Answer.create({answer: 'In ca. 100 m steht eine Stoptafel', correct: true, question: q5 })
Answer.create({answer: 'Ich muss in ca. 100 m anhalten', correct: true, question: q5 })
Answer.create({answer: 'Ich muss sofort anhalten', correct: false, question: q5 })
Answer.create({answer: 'In ca. 100 m steht das Verkehrszeichen "Vorrang geben"', correct: false, question: q5 })

q6 =Question.create({:title => 'Signs', :description => 'Was zeigt dieses Verkehrszeichen an?', :image_url => '6.jpg',  :category => c1})
Answer.create({answer: 'Eine Zufahrt zu einem Parkplatz', correct: false, question: q6 })
Answer.create({answer: 'Eine vorgeschriebene Fahrtrichtung', correct: true, question: q6 })
Answer.create({answer: 'Hier ist mit Gegenverkehr zu rechnen', correct: false, question: q6 })
Answer.create({answer: 'Den zu benützenden Fahrstreifen', correct: false, question: q6 })

q7 =Question.create({:title => 'Signs', :description => 'Was wird durch dieses Verkehrszeichen gekennzeichnet?', :image_url => '7.jpg',  :category => c1})
Answer.create({answer: 'Eine Gefahrenstelle, wo vermehrt Kinder sind', correct: false, question: q7 })
Answer.create({answer: 'Ein Gehweg', correct: true, question: q7 })
Answer.create({answer: 'Ein Gehsteig', correct: false, question: q7 })
Answer.create({answer: 'Eine Fußgängerunterführung', correct: false, question: q7 })

c2 = Category.create({:title => 'A: Motorcycle driving license'})
c3 = Category.create({:title => 'C: Truck driving license'})

u1 = User.create({:email => 'user@example.org', :password => 'test1234', :password_confirmation => 'test1234', :examdate => '2015-02-02'})
u1.categories << c1
u1.categories << c2
u1.save