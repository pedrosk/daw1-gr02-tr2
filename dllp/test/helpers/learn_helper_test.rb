require 'test_helper'

class LearnHelperTest < ActionView::TestCase

  def setup
    @user = User.first
    @question = Question.first
  end

  test "learn counter" do
    log_answer(@user, @question, true)

    attempt = AnswerAttempt.first
    assert attempt.correct == 1
    assert attempt.incorrect == 0
    assert attempt.practiced == 1


    log_answer(@user, @question, true)
    attempt = AnswerAttempt.first
    assert attempt.correct == 2
    assert attempt.incorrect == 0
    assert attempt.practiced == 2

    log_answer(@user, @question, false)
    attempt = AnswerAttempt.first
    assert attempt.correct == 0
    assert attempt.incorrect == 1
    assert attempt.practiced == 3
  end
end
