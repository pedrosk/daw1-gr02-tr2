require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "invalid category" do
    assert_not Category.new(:title => '').valid?
  end

  test "valid category" do
    cat = Category.create(:title => 'Test Category')
    assert Category.find(cat.id).title == 'Test Category'
  end
end
