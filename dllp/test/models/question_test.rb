require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.first
  end

  test "unanswered" do
    questions = Question.unanswered(@user)
    assert questions.count == 1
  end
end
