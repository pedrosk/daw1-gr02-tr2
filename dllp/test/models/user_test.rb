require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.first
  end

  test "valid user" do
    @user.password = "foobar123"
    @user.password_confirmation = "foobar123"
    assert @user.valid?
  end

  test "invalid mail" do
    @user.email = "foobar"
    assert_not @user.valid?
  end  

  test "invalid password" do
    @user.password = "foobar123"
    @user.password_confirmation = "foobar1234"
    assert_not @user.valid?
  end

  test "unique email" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end

  test "unique casesensitive email" do
    duplicate_user = @user.dup
    duplicate_user.email = duplicate_user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "auth success" do
    assert !!@user.authenticate("user1234")
  end

  test "auth failure"  do
    assert_not @user.authenticate("asdf1234")
  end

end
