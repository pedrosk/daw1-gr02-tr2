$(document).ready(function() {
	var ctx = $("#history-canvas")[0].getContext("2d");
	
	var data = {
		    labels: ["01.01.2015", "02.01.2015", "03.01.2015","04.01.2015","05.01.2015","06.01.2015","07.01.2015","08.01.2015",
		             "09.01.2015","10.01.2015","11.01.2015","12.01.2015","13.01.2015","14.01.2015",
		             "15.01.2015","16.17.2015","18.01.2015","19.01.2015"],
		    datasets: [
				{
				    label: "Correct",
				    fillColor: "rgba(90, 162, 204, 0.1)",
				    strokeColor: "rgb(90, 162, 204)",
				    pointColor: "rgb(90, 162, 204)",
				    pointStrokeColor: "#fff",
				    pointHighlightFill: "#fff",
				    pointHighlightStroke: "rgba(220,220,220,1)",
				    data: [10, 20, 30, 35, 33, 31, 40, 50, 60, 70, 75, 67, 80, 85, 90, 93, 95, 100]
				},
				{
				    label: "Learned",
				    fillColor: "rgba(164, 219, 121, 0.8)",
				    strokeColor: "rgb(164, 219, 121)",
				    pointColor: "rgb(164, 219, 121)",
				    pointStrokeColor: "#fff",
				    pointHighlightFill: "#fff",
				    pointHighlightStroke: "rgba(220,220,220,1)",
				    data: [0, 0, 3, 4, 10, 15, 20, 18, 19, 25, 30, 24, 36, 40, 38, 50, 60, 100]
				},
		        {
		            label: "Unanswered",
		            fillColor: "rgba(0,0,0,0)",
		            strokeColor: "rgba(0,0,0,1)",
		            pointColor: "rgba(0,0,0,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(151,187,205,1)",
				    data: [100, 98, 90, 87, 85, 80, 70, 68, 66, 63, 62, 61, 58, 55, 30, 20, 10, 0]
		        },
		        {
		            label: "Wrong",
		            fillColor: "rgb(239, 41, 41)",
		            strokeColor: "rgb(239, 41, 41)",
		            pointColor: "rgb(239, 41, 41)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(220,220,220,1)",
				    data: [10, 20, 30, 20, 15, 10, 5, 17, 25, 30, 20, 17, 16, 10, 5, 4, 2, 0]
		        },
		    ]
		};

	
	var historyChart = new Chart(ctx).Line(data, null);
	
});