$(document).ready(function() {

	animationduration = 400;

	$("#form-register").hide();
	$("#form-forgot").hide();
	$("#error-message").hide();
	$("#info-message").hide();
	$("#success-message").hide();

	$("#register-btn").click(function() {
		$("#form-register").show();
		$("#form-login").hide(animationduration);
	});

	$("#forgot-btn").click(function() {
		$("#form-forgot").show();
		$("#form-login").hide(animationduration);
	});

	$(".back-btn").click(function() {
		$("#form-forgot").hide(animationduration);
		$("#form-register").hide(animationduration);
		$("#form-login").show();
	});

	$("#login-form").submit(function(event) {
		$("#info-message").show(animationduration);
		$.post(baseUrl + '/login.php', $(this).serialize(), function (data) {
			$("#info-message").hide(animationduration);
			if(data != "ok") {
				$("#error-message").text(data);
				$("#error-message").show(animationduration);
			} else {
				window.location.href = baseUrl+"/main.html";
			}
			
		}).fail(function() {
			$("#info-message").hide(animationduration);
			$("#error-message").text("Unexpected error occured, please try again later.");
			$("#error-message").show(animationduration);
		});

		event.preventDefault();
	});
	
	$("#register-form").submit(function(event) {
		event.preventDefault();
		$("#error-message").hide(animationduration);
		
		if ($("#register-password").val() != $("#register-password2").val()) {
			$("#error-message").text("Your passwords do not match");
			$("#error-message").show(animationduration);
			return;
		}
		
		$("#info-message").show(animationduration);
		$.post(baseUrl + '/register.php', $(this).serialize(), function (data) {
			$("#info-message").hide(animationduration);
			if(data != "ok") {
				$("#error-message").text(data);
				$("#error-message").show(animationduration);
			} else {
				$("#success-message").text("Successfully registered, you can now signin.");
				$("#success-message").show(animationduration);
				$("#form-register").hide(animationduration);
				$("#form-login").show(animationduration);

			}
			
		}).fail(function() {
			$("#info-message").hide(animationduration);
			$("#error-message").text("Unexpected error occured, please try again later.");
			$("#error-message").show(animationduration);
		});

	});
});