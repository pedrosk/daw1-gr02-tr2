$(document).ready(function() {
	$("body").keypress(function(event) {
		switch (event.which) {
		case 49:
			if ($('input[name=answer1-cb]').is(':checked'))
				$('input[name=answer1-cb]').removeAttr('checked');
			else {
				$('input[name=answer1-cb]').attr('checked', 'checked');
			}
			break;
		case 50:
			if ($('input[name=answer2-cb]').is(':checked'))
				$('input[name=answer2-cb]').removeAttr('checked');
			else {
				$('input[name=answer2-cb]').attr('checked', 'checked');
			}
			break;
		case 51:
			if ($('input[name=answer3-cb]').is(':checked'))
				$('input[name=answer3-cb]').removeAttr('checked');
			else {
				$('input[name=answer3-cb]').attr('checked', 'checked');
			}
			break;
		case 52:
			if ($('input[name=answer4-cb]').is(':checked'))
				$('input[name=answer4-cb]').removeAttr('checked');
			else {
				$('input[name=answer4-cb]').attr('checked', 'checked');
			}
			break;
		case 13:
			checkAnswer();
			break;
		}
	});
	
	$("#btn-next").hide();
	
	$("#btn-next").click(function() {
		$("#btn-next").hide();
		$("#btn-check").show();
		$("#answer1").removeClass("callout-danger callout-success");
		$("#answer2").removeClass("callout-danger callout-success");
		$("#answer3").removeClass("callout-danger callout-success");
		$("#answer4").removeClass("callout-danger callout-success");

	});
	
	$("#btn-check").click(function(){
		checkAnswer();
	});

});

function checkAnswer() {
	$("#btn-check").hide();
	$("#btn-next").show();
	
	$("#answer1").addClass("callout-danger");
	$("#answer2").addClass("callout-success");
	$("#answer3").addClass("callout-success");
	$("#answer4").addClass("callout-danger");
}